#!/usr/bin/python 

import urllib2
import sys
import re
from time import time
from weakref import proxy
from optparse import OptionParser
from threading import BoundedSemaphore, Thread
from BeautifulSoup import BeautifulSoup

parser = OptionParser()
parser.add_option("-c","--maxthreads", dest='MAXTHREADS', help="Maximum Concurrency, Default is 50", default=50, type='int')
parser.add_option("-q","--query", dest="QUERY", help="String to Query For", default="", type="string")
parser.add_option("-n","--country", dest="COUNTRY", help="Country Code", default="us", type="string")
parser.add_option("-d","--debug", dest="DEBUG", action="store_true", help="Turn on Debug", default=False)
(options,args) = parser.parse_args()

## SANITY CHECK PARAMS 
def fail(func):
    def decorator(*args, **kwargs):
        print "A Required Parameter is Missing\n"
        return func(*args, **kwargs)
    return decorator
parser.print_help = fail(parser.print_help)
assert options.QUERY, parser.print_help()
## END SANITY CHECK

SEMAPHORE = BoundedSemaphore(options.MAXTHREADS)

class Craigslist_Search(Thread):
    instances = []
    queue = {} # Not Really a Queue, just a dict we're using to mimic uniq

    def __init__(self,link):
        self.__class__.instances.append(proxy(self)) # A list of weakrefs to class instances
        Thread.__init__(self)
        self.link = link

    def run(self):
        with SEMAPHORE:
            try:
                search = urllib2.urlopen('%s/search/?query=%s&catAbb=sss' % (self.link, options.QUERY))
                search_soup = BeautifulSoup(search)
                for item in search_soup.findAll('p'):
                    self.__class__.queue[item.findAll('a')[0]['href']] = 0 # Create a reference in the 'queue'
            except:
                pass # We don't care about error checking

    @classmethod # Why doesnt this already exist in python
    def joinall(cls, timeout=60): 
        '''Helper Method to Join Threads for All Class Instances'''
        for thread in cls.instances:
            try:
                thread.join(timeout)
            except ReferenceError:
                pass # Last one always throws a ReferenceError. Lets ignore it

starttime = time()

## Get a list of urls for CL sites
if options.COUNTRY == 'ALL':
    data = urllib2.urlopen('http://geo.craigslist.org/')
    soup = BeautifulSoup(data)
    links = soup.html.body.findAll('a')
    [ links.remove(link) for link in links if not link.has_key('href') ]
    href_regex = re.compile('^http://\D+\.craigslist\..*$')
    [ links.remove(link) for link in links if not href_regex.match(link['href']) ]
    
else:
    data = urllib2.urlopen('http://geo.craigslist.org/iso/%s' % options.COUNTRY).read()
    soup = BeautifulSoup(data)
    links = soup.html.body.findAll('a')[2:]

print "Searching %s %s Craigslist sites using %s Threads" %(len(links), options.COUNTRY.upper(), options.MAXTHREADS)

## Iterate through the List -- A new thread for each.
for link in links:
    if options.DEBUG: print 'Starting Thread for %s' %link['href']
    Craigslist_Search(link['href'].rstrip('/')).start()
## Wait for all threads to copmlete
Craigslist_Search.joinall(10)

## Dump output
print "%s items found" % len(Craigslist_Search.queue)
for k in sorted(Craigslist_Search.queue.iterkeys()):
    print k
print "Returned %s results in %.2f seconds" % (len(Craigslist_Search.queue), time() - starttime)
